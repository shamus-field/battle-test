﻿using Audrey;
using System;
using Microsoft.Xna.Framework;
using BattleTest.Components;

namespace BattleTest.AI
{
    class MapNodes
    {
        // takes all entities on a tile and figures out the max cost, for pathfinding calcs
        public byte[,] CalcMapNodes(Engine engine)
        {
            Family terrain = Family.All(typeof(TerrainComponent)).Get();
            ImmutableList<Entity> terrainEntities = engine.GetEntitiesFor(terrain);

            byte[,] grid = new byte[64, 64];
            foreach (Entity te in terrainEntities)
            {
                TerrainComponent nodeTerrain = te.GetComponent<TerrainComponent>();
                PositionComponent nodePos = te.GetComponent<PositionComponent>();

                Point gridPoint = nodePos.toTilePoint();
                grid[gridPoint.X, gridPoint.Y] = Math.Max(grid[gridPoint.X, gridPoint.Y], (byte)(nodeTerrain._cost));
            }
            return grid;
        }
    }
}
