﻿using System.Linq;
using Audrey;
using System.Diagnostics;
using System.Collections.Generic;
using Algorithms;
using Microsoft.Xna.Framework;
using BattleTest.Components;
using static BattleTest.Components.CombatComponent;
using BattleTest.AI;
using BattleTest.ScenarioData;
using System;

namespace BattleTest.AI
{
    class MapUtil
    {
        // was a test of gorogues algorithms - integrating the whole library in next version
        public List<Point> GetTilesInSquare(Point start, int size, int maxRange)
        {
            List<Point> points = new List<Point>();
            int xmin = Math.Max(0, start.X - size);
            int xmax = Math.Min(maxRange - 1, start.X + size);
            int ymin = Math.Max(0, start.Y - size);
            int ymax = Math.Min(maxRange - 1, start.Y + size);

            for (int y = ymin; y <= ymax; y++)
            {
                for (int x = xmin; x <= xmax; x++)
                {
                    points.Add(new Point(x, y));
                }
            }
            return points;
        }

        public static int Bound(int x, int max)
        {
            return (x < 0) ? 0 : (x > max - 1) ? max - 1 : x;
        }

        public List<Point> GetTilesInLine(Point start, Point end, int maxSize)
        {
            int startX = Bound(start.X, maxSize);
            int startY = Bound(start.Y, maxSize);
            int endX = Bound(end.X, maxSize);
            int endY = Bound(end.Y, maxSize);
            
            int dx = Math.Abs(endX - startX);
            int dy = Math.Abs(endY - startY);

            int sx = startX < endX ? 1 : -1;
            int sy = startY < endY ? 1 : -1;
            int err = dx - dy;

            List<Point> points = new List<Point>();
            while (true)
            {
                if ((startX >= 0 && startX < maxSize) && (startY >= 0 && startY < maxSize))
                {
                    points.Add(new Point(startX, startY));
                }

                if (startX == endX && startY == endY)
                {
                    break;
                }

                int e2 = 2 * err;
                if (e2 > -dy)
                {
                    err = err - dy;
                    startX = startX + sx;
                }
                if (e2 < dx)
                {
                    err = err + dx;
                    startX = startX + sy;
                }

            }
            return points;
        }


        // use bresenhams circle algorithm to get the tiles
        public List<Point> GetTilesInRange(Point asTile, int range, int maxSize)
        {
            List<Point> pointList = new List<Point>();
            int d = (5 - range * 4) / 4;
            int x = 0;
            int y = range;

            do
            {
                pointList.AddRange(GetTilesInLine(
                    new Point(asTile.X + x, asTile.Y + y),
                    new Point(asTile.X - x, asTile.Y + y),
                    maxSize));
                pointList.AddRange(GetTilesInLine(
                    new Point(asTile.X - x, asTile.Y - y),
                    new Point(asTile.X + x, asTile.Y - y),
                    maxSize));
                pointList.AddRange(GetTilesInLine(
                    new Point(asTile.X + y, asTile.Y + x),
                    new Point(asTile.X - y, asTile.Y + x),
                    maxSize));
                pointList.AddRange(GetTilesInLine(
                    new Point(asTile.X + y, asTile.Y - x),
                    new Point(asTile.X - y, asTile.Y - x),
                    maxSize));

                if (d < 0)
                {
                    d += 2 * x + 1;
                }
                else
                {
                    d += 2 * (x - y) + 1;
                    y--;
                }
                x++;
            } while (x <= y);

           
            return pointList.Distinct().ToList();
        }

    }
}
