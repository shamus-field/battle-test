﻿using System.Linq;
using Audrey;
using System.Diagnostics;
using System.Collections.Generic;
using Algorithms;
using Microsoft.Xna.Framework;
using BattleTest.Components;
using static BattleTest.Components.CombatComponent;
using BattleTest.AI;
using BattleTest.ScenarioData;

// keeping for reference, moved onto AI2
namespace BattleTest
{
    // enemy in sight
    // can attack? then attack
    // no? move
    // else move to objective
    public class UnitAI
    {
        public void RunCombatAI(Engine engine, GameTime gameTime, byte[,] mapGrid,
            Dictionary<Entity, Vector2[,]> spawnFlowFields)
        {
            Family combatants = Family.All(typeof(CombatComponent)).Get();
            ImmutableList<Entity> combatEntities = engine.GetEntitiesFor(combatants);
            List<Entity> removeEntities = new List<Entity>();

            foreach (Entity e in combatEntities)
            {
                CombatComponent comp = e.GetComponent<CombatComponent>();
                PositionComponent pos = e.GetComponent<PositionComponent>();
                ActionComponent actions = e.GetComponent<ActionComponent>();

                if (comp._slain)
                {
                    continue;
                }

                if (comp._enemyTarget != null)
                {
                    CombatComponent targetComp = comp._enemyTarget.GetComponent<CombatComponent>();
                    if (targetComp._slain)
                    {
                        comp._enemyTarget = null;
                        comp._mode = comp._originalMode;
                        comp._currentPath = null;
                        comp.currentFlowTarget = Vector2.Zero;
                    }
                }

                if (comp._mode == Mode.Move || comp._mode == Mode.Guard)
                {
                    if (comp.possibleTargets.Count == 0)
                    {
                        if (comp._turnTicks > 0)
                        {
                            comp._turnTicks -= 1;
                        }
                        else
                        {
                            comp._turnTicks = comp._startTurnTicks;
                            comp.possibleTargets = FindPossibleTargets(comp._enemies, e, combatEntities);
                        }
                    }

                    Entity target = null;
                    for (int i = comp.possibleTargets.Count - 1; i >= 0; i--)
                    {
                        Entity checkEntity = comp.possibleTargets[i];
                        CombatComponent targetComp = checkEntity.GetComponent<CombatComponent>();
                        if (!targetComp._slain)
                        {
                            target = checkEntity;
                            break;
                        }
                        else
                        {
                            comp.possibleTargets.RemoveAt(i);
                        }
                    }

                    //Entity target = FindAnyTarget(comp._enemies, e, combatEntities);
                    //Entity target = FindNearestTarget(comp._enemies, e, combatEntities);
                    if (target != null)
                    {
                        comp._mode = Mode.Attack;
                        comp._enemyTarget = target;
                    }
                    else if (comp._mode == Mode.Move)
                    {
                        if (comp.currentFlowTarget == Vector2.Zero)
                        {
                            Vector2 posGridVector = pos.getGridCenterVector();
                            Vector2 targetPoint = spawnFlowFields[comp.originalTarget][(int)posGridVector.X, (int)posGridVector.Y];
                            comp.currentFlowTarget = targetPoint;
                        }

                        Vector2 unitCenter = new Vector2(pos._point.X + 16, pos._point.Y + 16);

                        //measure against target point -16,-16 here
                        if (Vector2.Distance(comp.currentFlowTarget, unitCenter) >= 1)
                        {
                            Vector2 dir = comp.currentFlowTarget - unitCenter;
                            dir.Normalize();
                            pos._point = pos._point + dir * comp._speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        }
                        else
                        {

                            comp.currentFlowTarget = Vector2.Zero;
                        }
                    }
                    else
                    {
                        //guard, do nothing
                    }
                }

                if (comp._enemyTarget != null)
                {

                    PositionComponent targetPos = comp._enemyTarget.GetComponent<PositionComponent>();
                    CombatComponent targetComp = comp._enemyTarget.GetComponent<CombatComponent>();
                    ////Debug.WriteLine(targetPos._point);
                    ////Debug.WriteLine(targetComp._curHealth);
                    ////Debug.WriteLine(targetComp._faction);
                    UnitAction chosen = actions.ChooseAction();

                    float distance = Vector2.Distance(pos._point, targetPos._point);
                    ////Debug.WriteLine(distance);
                    //Entity nearest = FindAnyTarget(comp._enemies, e, combatEntities);
                    ////Entity nearest = FindNearestTarget(comp._enemies, e, combatEntities);
                    //if (nearest != null)
                    //{
                    //    PositionComponent nearestPos = nearest.GetComponent<PositionComponent>();
                    //    float nearestDistance = Vector2.Distance(pos._point, nearestPos._point);
                    //    if (nearestDistance < distance)
                    //    {
                    //        comp._enemyTarget = nearest;
                    //        comp._currentPath = null;
                    //        targetPos = nearestPos;
                    //        targetComp = nearest.GetComponent<CombatComponent>();
                    //        distance = nearestDistance;
                    //    }
                    //}



                    if (distance < chosen._range * targetComp._size)
                    {
                        if (comp._actionTicks > 0)
                        {
                            comp._actionTicks -= 1;
                            continue;
                        }
                        else
                        {
                            comp._actionTicks = comp._startActionTicks;
                            //attack
                            chosen.Perform(engine, e, new Entity[1] { comp._enemyTarget });
                        }
                    }
                    else
                    {
                        // if path && target hasn't moved, use that
                        if (comp._currentPath != null && comp._currentPath.Count != 0)
                        {
                            PathFinderNode pathEnd = comp._currentPath[comp._currentPath.Count - 1];
                            Vector2 v = new Vector2(pathEnd.X * 32, pathEnd.Y * 32);
                            float lastPathDistance = Vector2.Distance(v, targetPos._point);

                            if (lastPathDistance <= 20)
                            {
                                PathFinderNode moveTo = comp._currentPath[0];
                                Vector2 moveToV = new Vector2(moveTo.X * 32, moveTo.Y * 32);

                                Vector2 norm = (moveToV - pos._point);
                                norm.Normalize();
                                ////Debug.WriteLine(norm);
                                pos._point = pos._point + norm * comp._speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                                //pos._point = pos._point + norm * comp._speed;

                                float moveToDistance = Vector2.Distance(pos._point, moveToV);
                                if (moveToDistance <= 10)
                                {
                                    comp._currentPath.RemoveAt(0);
                                }
                            }
                            else
                            {
                                comp._currentPath = null;
                            }
                        }


                        //else calc new path

                        if (comp._currentPath == null)
                        {
                            //if the target moved, check to see if any other targets are closer now
                            //Entity target = FindAnyTarget(comp._enemies, e, combatEntities);
                            //Entity target = FindNearestTarget(comp._enemies, e, combatEntities);
                            //comp._enemyTarget = target;
                            targetPos = comp._enemyTarget.GetComponent<PositionComponent>();

                            Point start = pos.toCenterTilePoint();
                            Point end = targetPos.toCenterTilePoint();

                            //Point closestTargetNeighbor = FindClosestTargetNeighbor(pos, targetPos);

                            PathFinderFast finder = new PathFinderFast(mapGrid);
                            finder.SearchLimit = 64 * 64 * 2;
                            List<PathFinderNode> path = finder.FindPath(start, end);

                            if (path != null)
                            {
                                path.RemoveAt(path.Count - 1);

                                path.Reverse();
                                comp._currentPath = path;
                            }

                        }
                    }

                    if (targetComp._curHealth <= 0)
                    {
                        targetComp._slain = true;
                        removeEntities.Add(comp._enemyTarget);
                    }

                }
                else
                {
                    if (comp._originalMode != Mode.Guard)
                    {
                        comp._mode = Mode.Move;
                    }

                }
            }

            foreach (Entity e in removeEntities)
            {
                engine.DestroyEntity(e);
            }
            removeEntities.Clear();
        }

        // for a given start/end, find the closest neighbor of the end point
        // to the start point
        public Point FindClosestTargetNeighbor(PositionComponent start, PositionComponent end)
        {
            Vector2 startCenter = start.getCenterVector();
            Vector2 endCenter = end.getCenterVector();

            Vector2 left = endCenter + new Vector2(-32, 0);
            Vector2 right = endCenter + new Vector2(32, 0);
            Vector2 up = endCenter + new Vector2(0, -32);
            Vector2 down = endCenter + new Vector2(0, 32);

            return new Point();
        }

        public List<Entity> FindPossibleTargets(string[] factions, Entity actor,
            ImmutableList<Entity> combatants)
        {
            //only keep 15 at a time to prevent running through a huge list of guys
            List<Entity> targets = new List<Entity>();

            PositionComponent actorPos = actor.GetComponent<PositionComponent>();
            CombatComponent actorComp = actor.GetComponent<CombatComponent>();

            int i = 0;
            foreach (Entity e in combatants)
            {
                CombatComponent comp = e.GetComponent<CombatComponent>();
                if (comp._slain)
                {
                    continue;
                }
                if (!factions.Contains(comp._faction))
                {
                    continue;
                }

                PositionComponent pos = e.GetComponent<PositionComponent>();

                float distance = Vector2.Distance(pos._point, actorPos._point);
                if (distance < actorComp._sightRange)
                {
                    i += 1;
                    targets.Add(e);
                }

                if (i >= 14)
                {
                    return targets;
                }
            }
            return targets;
        }

        // snapshot all nearby targets once, and just iterate through those
        //when the list is empty scan again
        //limit scans to once/second?
        public Entity FindAnyTarget(string[] factions, Entity actor,
            ImmutableList<Entity> combatants)
        {
            PositionComponent actorPos = actor.GetComponent<PositionComponent>();
            CombatComponent actorComp = actor.GetComponent<CombatComponent>();

            foreach (Entity e in combatants)
            {
                CombatComponent comp = e.GetComponent<CombatComponent>();
                if (comp._slain)
                {
                    continue;
                }

                if (!factions.Contains(comp._faction))
                {
                    continue;
                }

                PositionComponent pos = e.GetComponent<PositionComponent>();

                float distance = Vector2.Distance(pos._point, actorPos._point);
                if (distance < actorComp._sightRange)
                {
                    return e;
                }
            }
            return null;
        }

        // maybe check nearby first before looping all?
        public Entity FindNearestTarget(string[] factions, Entity actor,
            ImmutableList<Entity> combatants)
        {
            PositionComponent actorPos = actor.GetComponent<PositionComponent>();
            CombatComponent actorComp = actor.GetComponent<CombatComponent>();

            Entity currentTarget = null;
            float lowestDistance = float.MaxValue;

            foreach (Entity e in combatants)
            {
                CombatComponent comp = e.GetComponent<CombatComponent>();
                if (comp._slain)
                {
                    continue;
                }

                if (!factions.Contains(comp._faction))
                {
                    continue;
                }

                PositionComponent pos = e.GetComponent<PositionComponent>();

                float distance = Vector2.Distance(pos._point, actorPos._point);
                if (distance < actorComp._sightRange && distance < lowestDistance)
                {
                    lowestDistance = distance;
                    currentTarget = e;
                }
                else
                {
                    continue;
                }

            }

            return currentTarget;
        }
    }
}
