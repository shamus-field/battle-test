﻿using Audrey;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleTest.Components;


namespace BattleTest.AI
{
    // keeps track of all unit locations in the game
    class UnitMap
    {
        public Entity[,] _unitMap;

        public UnitMap()
        {
            _unitMap = new Entity[64, 64];
        }

        public void AddUnit(Entity e, PositionComponent pos)
        {
            Point asTilePoint = pos.toTilePoint();
            _unitMap[asTilePoint.X, asTilePoint.Y] = e;
        }

        public void UpdateUnit(PositionComponent oldPos, PositionComponent newPos)
        {
            Point oldPoint = oldPos.toTilePoint();
            Point newPoint = newPos.toTilePoint();
            _unitMap[newPoint.X, newPoint.Y] = _unitMap[oldPoint.X, oldPoint.Y];
            _unitMap[oldPoint.X, oldPoint.Y] = null;
        }

        public void RemoveUnit(PositionComponent pos)
        {
            Point point = pos.toTilePoint();
            _unitMap[point.X, point.Y] = null;
        }
    }
}
