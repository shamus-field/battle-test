﻿using Audrey;
using BattleTest.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BattleTest.ScenarioData;

namespace BattleTest.AI
{
    // periodically triggers all spawners on the battlefield
    class UnitSpawner
    {
        float _spawnTime;

        public UnitSpawner(float spawnTime = 30.0f)
        {
            _spawnTime = spawnTime;
        }

        public void HandleSpawns(Engine engine, GameTime gameTime,
            Dictionary<string, Unit> unitDict, Dictionary<string, Faction> factionDict,
            UnitMap unitMap)
        {
            Family spawners = Family.One(typeof(SpawnComponent)).Get();
            ImmutableList<Entity> spawnEntities = engine.GetEntitiesFor(spawners);

            foreach (Entity e in spawnEntities)
            {
                SpawnComponent spawn = e.GetComponent<SpawnComponent>();
                spawn._currentTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (spawn._currentTimer >= spawn._spawnTimer)
                {
                    Random rnd = new Random();
                    for (int i = 0; i < 10; i++)
                    {
                        int r = rnd.Next(spawn._spawnPoints.Count());
                        int r2 = rnd.Next(spawn._spawnOptions.Count());

                        Entity s = engine.CreateEntity();
                        Unit toSpawn = unitDict[spawn._spawnOptions[r2]].getCopy();

                        s.AddComponent(toSpawn._img);
                        s.AddComponent(toSpawn._action);
                        s.AddComponent(toSpawn._combat);
                        PositionComponent pos = new PositionComponent(
                            spawn._spawnPoints[r].ToPoint());
                        s.AddComponent(pos);

                        Point tilePoint = pos.toTilePoint();
                        unitMap.AddUnit(e, pos);

                        CombatComponent comb = s.GetComponent<CombatComponent>();
                        comb.originalTarget = spawn._spawnTargets[0];
                        comb._originalMode = CombatComponent.Mode.Move;

                        Faction faction = factionDict[spawn._faction];
                        comb._enemies = faction._enemies;
                        comb._allies = faction._allies;
                        comb._neutral = faction._neutral;
                        comb._faction = spawn._faction;
                    }

                    spawn._currentTimer -= spawn._spawnTimer;
                }


            }


        }

    }
}
