﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

using Audrey;
using Terrain;
using System.Diagnostics;
using BattleTest.AI;
using BattleTest.Components;
using BattleTest.Processors;
using BattleTest.TestScenarios;
using BattleTest.Utilities;
using BattleTest.ScenarioData;
using BattleTest.Maps;
using BattleTest.State;


// OVERALL TODOS
//
// clean up factions a bit - shoulnd't need them on the combat component
// upper case all method names - mostly the point stuff is left
// only do draw calls on units in viewport (research - does monogame automatically do this for you?)
// allow for view scrolling
namespace BattleTest
{
    public class BattleTest : Game
    {
        BattleState battleState;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Engine engine;
        UnitAI ai;
        UnitAI2Processor ai2;
        UnitSpawner spawner;
        ProjectileProcessor projProcessor;
        public bool paused = false;
        private SpriteFont font;
        private MouseState mouseState;
        private Dictionary<string, Texture2D> utilTextures;
        private byte[,] grid;
        private UnitMap unitMap;
        private Dictionary<Entity, Vector2[,]> spawnFlowFields;
        public MapManager mapManager;
        private FPSCounter _frameCounter = new FPSCounter();

        private Dictionary<string, UnitAction> actionDict;
        private Dictionary<string, Unit> unitDict;
        private Dictionary<string, Faction> factionDict;

        // DEBUG - to test flow fields
        private Vector2[,] testGrid2;

        public BattleTest()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1600;
            graphics.PreferredBackBufferHeight = 900;
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            engine = new Engine();
            
            base.Initialize();
            this.IsMouseVisible = true;

            // load json files containing unit/scenario data
            string unitActionJSON = System.IO.File.ReadAllText(@"C:\Users\Shamus\source\repos\BattleTest\BattleTest\Data\unit_actions.json");
            UnitActionJSONLoader uaLoader = new UnitActionJSONLoader();
            actionDict = uaLoader.loadJSONData(unitActionJSON, engine);

            string unitJSON = System.IO.File.ReadAllText(@"C:\Users\Shamus\source\repos\BattleTest\BattleTest\Data\unit_data.json");
            UnitJSONLoader uLoader = new UnitJSONLoader();
            unitDict = uLoader.loadJSONData(unitJSON, engine, actionDict);

            string scenarioJSON = System.IO.File.ReadAllText(@"C:\Users\Shamus\source\repos\BattleTest\BattleTest\Data\Scenarios\spawn_test.json");
            ScenarioJSONLoader sLoader = new ScenarioJSONLoader();
            factionDict = sLoader.loadFactionJSONData(scenarioJSON);

            mapManager = new MapManager(64);

            // create processors that run during update()
            projProcessor = new ProjectileProcessor();
            spawner = new UnitSpawner();
            ai = new UnitAI();
            ai2 = new UnitAI2Processor();
            spawnFlowFields = new Dictionary<Entity, Vector2[,]>();

            // DEBUG - just spawn 64x64 grass tiles for now
            for (int x = 0; x < 64; x++)
            {
                for (int y = 0; y < 64; y++)
                {
                    TerrainEntities.Grass(engine, new PositionComponent(32 * x, 32 * y));
                }
            }

            unitMap = new UnitMap();

            // DEBUG - assorted test scenarios
            TestScenariosTypes sc = new TestScenariosTypes();
            //sc.AStarTest(engine);
            //sc.FlowFieldTest(engine);
            //sc.SpawnTest(engine, unitDict, factionDict);


            // this will start a battle
            battleState = new BattleState(engine, mapManager, unitDict, actionDict, factionDict);
            sc.AI2Test(battleState);

            // calc movement costs based on test scenario terrain placements
            mapManager.CalcTerrainCosts(engine);

            // DEBUG - experimental spawning system, may ditch
            /*
            MapNodes mapper = new MapNodes();
            grid = mapper.CalcMapNodes(engine);

            Family spawners = Family.One(typeof(SpawnComponent)).Get();
            ImmutableList<Entity> spawnEntities = engine.GetEntitiesFor(spawners);
            foreach (Entity e in spawnEntities)
            {
                SpawnComponent spawn = e.GetComponent<SpawnComponent>();
                PositionComponent targetPos = spawn._spawnTargets[0].GetComponent<PositionComponent>();

                FlowField targetField = new FlowField(grid);
                spawnFlowFields[e] = targetField.FindPaths(spawn._enemyTargetPoint);
                testGrid2 = targetField.FindPaths(spawn._enemyTargetPoint);
            }
            */
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            utilTextures = new Dictionary<string, Texture2D>();

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            engine.contentMap["grass1.png"] = this.Content.Load<Texture2D>("Terrain\\grass1");
            engine.contentMap["dwarf.png"] = this.Content.Load<Texture2D>("Units\\dwarf");
            engine.contentMap["acid_blob.png"] = this.Content.Load<Texture2D>("Units\\acid_blob");
            engine.contentMap["tree2_red.png"] = this.Content.Load<Texture2D>("Terrain\\tree2_red");
            engine.contentMap["corner.png"] = this.Content.Load<Texture2D>("Terrain\\corner");
            engine.contentMap["floor.png"] = this.Content.Load<Texture2D>("Terrain\\floor");
            engine.contentMap["side.png"] = this.Content.Load<Texture2D>("Terrain\\side");
            engine.contentMap["crossbow_bolt.png"] = this.Content.Load<Texture2D>("Projectiles\\bolt0");

            font = Content.Load<SpriteFont>("sample_font");

            Texture2D greenYellowPixel = new Texture2D(graphics.GraphicsDevice, 1, 1);
            greenYellowPixel.SetData(new Color[] { Color.GreenYellow });
            utilTextures["green_yellow"] = greenYellowPixel;

            Texture2D redPixel = new Texture2D(graphics.GraphicsDevice, 1, 1);
            redPixel.SetData(new Color[] { Color.Red });
            utilTextures["red"] = redPixel;

            Texture2D whitePixel = new Texture2D(graphics.GraphicsDevice, 1, 1);
            whitePixel.SetData(new Color[] { Color.White });
            utilTextures["white"] = whitePixel;
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            mouseState = Mouse.GetState();

            // pause to get a good overview of the battlefield (OK it's really for debugging)
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (paused)
                {
                    paused = false;
                }
                else
                {
                    paused = true;
                    return;
                }
            }

            if (paused)
            {
                return;
            }

            // TODO - reduce frequency to once/second MAX
            spawner.HandleSpawns(engine, gameTime, unitDict, factionDict, unitMap);

            // DEBUG - running experimental ai2
            /*
            using (var bench = new Benchmark("ai"))
            {
                //ai.RunCombatAI(engine, gameTime, grid, spawnFlowFields);
            }
            */
            using (var bench = new Benchmark("ai2"))
            {
                ai2.RunCombatAI(battleState);
            }
            using (var bench = new Benchmark("proj"))
            {
                projProcessor.processProjectiles(engine, gameTime);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            spriteBatch.Begin();

            // draw all position/texture entities (terrain + actors)
            Family draws = Family.All(typeof(ImageComponent), typeof(PositionComponent)).Get();
            ImmutableList<Entity> drawEntities = engine.GetEntitiesFor(draws);
            //Debug.WriteLine("Count of entities" + drawEntities.Count);
            foreach (Entity e in drawEntities)
            {
                ImageComponent img = e.GetComponent<ImageComponent>();
                PositionComponent pos = e.GetComponent<PositionComponent>();

                // angle is important for projectiles
                if (pos._angle != default(float))
                {
                    spriteBatch.Draw(img._texture, new Vector2(pos._point.X + 16, pos._point.Y + 16), null, null,
                        new Vector2(16, 16), pos._angle, null, null,
                        SpriteEffects.None, 0.0f);
                }
                else
                {
                    spriteBatch.Draw(img._texture, pos._point);
                }
            }

            // draw all projectiles
            Family projectiles = Family.All(typeof(ProjectileComponent)).Get();
            ImmutableList<Entity> projEntities = engine.GetEntitiesFor(projectiles);
            foreach (Entity e in projEntities)
            {
                ImageComponent img = e.GetComponent<ImageComponent>();
                ProjectileComponent proj = e.GetComponent<ProjectileComponent>();
                spriteBatch.Draw(img._texture, proj._pos, null, null, new Vector2(16, 16),
                    (float)proj._angle, null, null, SpriteEffects.None, 0.0f);
            }

            // draw unit health bars
            Family unitHealthBars = Family.All(typeof(CombatComponent)).Get();
            ImmutableList<Entity> unitHealthEntities = engine.GetEntitiesFor(unitHealthBars);
            foreach (Entity e in unitHealthEntities)
            {
                PositionComponent pos = e.GetComponent<PositionComponent>();
                CombatComponent comb = e.GetComponent<CombatComponent>();

                Vector2 healthBarPos = new Vector2(pos._point.X + 6, pos._point.Y + 30);
                float pctLeft = comb._curHealth / (float)comb._maxHealth;
                int greenPixels = (int)(pctLeft * 20);
                int redPixels = (int)((1 - pctLeft) * 20);

                // DEBUG - draw unit current path
                /*
                if (comb._currentPath != null)
                {
                    foreach (PathFinderNode node in comb._currentPath)
                    {
                        Rectangle pfNode = new Rectangle(node.X * 32 + 16, node.Y * 32 + 16, 2, 2);
                        spriteBatch.Draw(utilTextures["green_yellow"], pfNode, Color.GreenYellow);
                    }
                }
                */

                Rectangle green = new Rectangle((int)healthBarPos.X, (int)healthBarPos.Y, greenPixels, 2);
                spriteBatch.Draw(utilTextures["green_yellow"], green, Color.GreenYellow);

                Rectangle red = new Rectangle((int)healthBarPos.X + greenPixels, (int)healthBarPos.Y, redPixels, 2);
                spriteBatch.Draw(utilTextures["red"], red, Color.Red);
            }

            // DEBUG - print movement costs of tiles
            /*
            for (int x = 0; x < mapManager._combinedMap.GetLength(0); x++)
            {
                for (int y = 0; y < mapManager._combinedMap.GetLength(1); y++)
                {
                    spriteBatch.DrawString(font, mapManager._combinedMap[x, y].ToString(),
                        new Vector2(x * 32 + 10, y * 32 + 10), Color.Gold);
                }
            }
            */

            // draw grid
            for (int i = 0; i < 64; ++i)
            {
                Rectangle rectangle = new Rectangle(i * 32, 0, 1, 1080);
                spriteBatch.Draw(utilTextures["white"], rectangle, Color.White);
            }
            for (int i = 0; i < 64; ++i)
            {
                Rectangle rectangle = new Rectangle(0, i * 32, 1920, 1);
                spriteBatch.Draw(utilTextures["white"], rectangle, Color.White);
            }

            // DEBUG - draw mouse coords
            /*
            int mouseX = mouseState.X / 32;
            int mouseY = mouseState.Y / 32;
            spriteBatch.DrawString(font, mouseState.X + " " + mouseState.Y, new Vector2(400, 400), Color.GreenYellow);
            spriteBatch.DrawString(font, Math.Floor(mouseState.X / 32.0) + " " + Math.Floor(mouseState.Y / 32.0), new Vector2(400, 420), Color.GreenYellow);
            spriteBatch.DrawString(font, Math.Floor(mouseState.X / 32.0) * 32 + " " + Math.Floor(mouseState.Y / 32.0) * 32, new Vector2(400, 440), Color.GreenYellow);
            */
            
            // DEBUG - visualize a get tiles in range call
            /*
            Point test = new Point(24, 14);
            //List<Point> testList = new MapUtil().GetTilesInRange(test, 2);
            List<Point> testList = new MapUtil().GetTilesInSquare(test, 1, 64);
            Debug.WriteLine(testList);
            for (int i = 0; i < testList.Count; i++)
            {
                Point p = testList[i];
                //Debug.WriteLine(p);
                Rectangle green = new Rectangle((int)p.X * 32 + 16, (int)p.Y * 32 + 16, 5, 2);
                spriteBatch.Draw(utilTextures["green_yellow"], green, Color.GreenYellow);
            }
            */

            // display fps in upper left corner
            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            _frameCounter.Update(deltaTime);
            var fps = string.Format("FPS: {0}", _frameCounter.AverageFramesPerSecond);
            spriteBatch.DrawString(font, fps, new Vector2(40, 40), Color.Black);

            spriteBatch.End();
            base.Draw(gameTime);

            stopwatch.Stop();
        }
    }
}
