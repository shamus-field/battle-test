﻿using Audrey;
using BattleTest.AI;
using BattleTest.ScenarioData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleTest.Components
{
    // holds a series of possible unit actions
    public class ActionComponent : IComponent
    {
        public List<KeyValuePair<UnitAction, float>> actions { get; set; }

        public UnitAction ChooseAction()
        {
            Random r = new Random();
            double roll = r.NextDouble();

            double c = 0.0;
            for (int i = 0; i < actions.Count; i++)
            {
                c += actions[i].Value;
                if (roll < c)
                {
                    return actions[i].Key;
                }
            }
            throw new System.ArgumentException("Actions do not add up to 1");
        }
    }
}
