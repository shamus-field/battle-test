﻿using Audrey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleTest.Components
{
    // health for buildings (will operate different than units probably)
    public class BuildingComponent : IComponent
    {
        public int _maxHealth { get; set; }
        public int _curHealth { get; set; }
        public string _faction { get; set; }

        public BuildingComponent(int maxHealth, string faction)
        {
            _maxHealth = maxHealth;
            _curHealth = maxHealth;
            _faction = faction;
        }
    }
}
