﻿using Microsoft.Xna.Framework.Graphics;

using Audrey;
using System.Collections.Generic;
using Algorithms;
using BattleTest.AI;
using Microsoft.Xna.Framework;

namespace BattleTest.Components
{
    // holds anything related to combat
    public class CombatComponent : IComponent
    {
        public enum Mode { Guard, Attack, Move }
        public enum TargetRelation { Enemy, Ally, Neutral }

        public int _maxHealth;
        public int _curHealth;
        public int _sightRange;
        public int _size;
        public int _speed;
        public int _turnTicks;
        public int _startTurnTicks;
        public int _actionTicks;
        public int _startActionTicks;
        public string _faction;
        public string[] _enemies;
        public string[] _allies;
        public string[] _neutral;
        public Mode _mode;
        public Mode _originalMode = Mode.Guard;
        public Entity _enemyTarget;
        public bool _slain = false;
        public List<PathFinderNode> _currentPath;
        public Vector2 currentFlowTarget = Vector2.Zero;
        public Entity originalTarget { get; set; }
        public List<Entity> possibleTargets = new List<Entity>();

        //no team set later
        public CombatComponent(int maxHealth, int sightRange, int size, int speed,
            int turnTicks, int actionTicks)
        {
            _maxHealth = maxHealth;
            _curHealth = maxHealth;
            _sightRange = sightRange;
            _size = size;
            _speed = speed;
            _turnTicks = turnTicks;
            _startTurnTicks = turnTicks;
            _actionTicks = actionTicks;
            _startActionTicks = actionTicks;
        }

        public CombatComponent(int maxHealth, int sightRange, int size, string faction,
            int speed, int turnTicks, int actionTicks)
        {
            _maxHealth = maxHealth;
            _curHealth = maxHealth;
            _sightRange = sightRange;
            _size = size;
            _speed = speed;
            _turnTicks = turnTicks;
            _startTurnTicks = turnTicks;
            _actionTicks = actionTicks;
            _startActionTicks = actionTicks;
            _faction = faction;

        }

        public CombatComponent(int maxHealth, int sightRange, int size, string faction,
            string[] enemies, string[] allies, string[] neutral, int speed, int turnTicks,
            int actionTicks)
        {
            _maxHealth = maxHealth;
            _curHealth = maxHealth;
            _sightRange = sightRange;
            _size = size;
            _speed = speed;
            _faction = faction;
            _enemies = enemies;
            _allies = allies;
            _neutral = neutral;
            _turnTicks = turnTicks;
            _startTurnTicks = turnTicks;
            _actionTicks = actionTicks;
            _startActionTicks = actionTicks;
        }

        public string[] getTargetRelation(TargetRelation targetRelation)
        {
            if (targetRelation == TargetRelation.Ally)
            {
                return _allies;
            }
            else if (targetRelation == TargetRelation.Enemy)
            {
                return _enemies;
            }
            else
            {
                return _neutral;
            }
        }
    }
}
