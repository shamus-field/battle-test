﻿using Microsoft.Xna.Framework.Graphics;

using Audrey;

namespace BattleTest.Components
{
    // holds the texture to render an entity
    public class ImageComponent : IComponent
    {
        public Texture2D _texture;

        public ImageComponent(Texture2D texture)
        {
            _texture = texture;
        }
    }
}
