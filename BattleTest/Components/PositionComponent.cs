﻿using Audrey;
using System;
using Microsoft.Xna.Framework;

namespace BattleTest.Components
{
    // holds position info for any entity that will be placed in the world
    // has some utility point methods which will be removed into a util lib later
    // also migrating to use go rogues coord system which will eliminate some of 
    // the duplicates between vectors/points/etc
    public class PositionComponent : IComponent
    {
        public Vector2 _point;
        public float _angle { get; set; }

        public PositionComponent(float x, float y)
        {
            _point = new Vector2(x, y);
        }

        public PositionComponent(float x, float y, float angle)
        {
            _point = new Vector2(x, y);
            _angle = angle;
        }

        public PositionComponent(Point gridSquare)
        {
            _point = new Vector2(gridSquare.X * 32, gridSquare.Y * 32);
        }

        public PositionComponent(Point gridSquare, float angle)
        {
            _point = new Vector2(gridSquare.X * 32, gridSquare.Y * 32);
            _angle = angle;
        }

        public void SetFromTilePoint(Point tilePoint)
        {
            _point = new Vector2(tilePoint.X * 32, tilePoint.Y * 32);
        }

        public Vector2 getGridVector()
        {
            return new Vector2(_point.X / 32, _point.Y / 32);
        }

        public Vector2 getGridCenterVector()
        {
            return new Vector2((_point.X + 16) / 32, (_point.Y + 16) / 32);
        }

        public Vector2 getCenterVector()
        {
            return new Vector2(((int)_point.X / 32) * 32 + 16, ((int)_point.Y / 32) * 32 + 16);
        }

        public Point toPoint()
        {
            Point point = new Point((int)_point.X, (int)_point.Y);
            return point;
        }

        public Point toTilePoint()
        {
            Point point = new Point((int)_point.X / 32, (int)_point.Y / 32);
            return point;
        }

        public Point toCenterTilePoint()
        {
            Point point = new Point((int)((_point.X + 16) / 32), (int)((_point.Y + 16) / 32));
            return point;
        }
    }
}
