﻿using Microsoft.Xna.Framework;
using Audrey;
using System;
using System.Diagnostics;

namespace BattleTest.Components
{
    // holds any projectile info
    public class ProjectileComponent : IComponent
    {
        public Vector2 _source { get; set; }
        public Vector2 _pos { get; set; }
        public Vector2 _target { get; }
        public int _speed { get; }
        public double _angle { get; }
        public Vector2 _normalized { get; }
        public int _turnTicks { get; set; }
        public int _startTurnTicks { get; set; }

        public ProjectileComponent(float curX, float curY, float targetX,
            float targetY, int speed = 200)
        {
            _source = new Vector2(curX, curY);
            _pos = new Vector2(curX, curY);
            _target = new Vector2(targetX, targetY);
            _speed = speed;
            _startTurnTicks = 60;
            _turnTicks = 60;

            Vector2 norm = (_target - _pos);
            norm.Normalize();
            _normalized = norm;
            _angle = Math.Atan2(_normalized.Y, _normalized.X) + (Math.PI / 2);
        }
    }
}
