﻿using System;
using System.Collections.Generic;
using Audrey;
using BattleTest.ScenarioData;
using Microsoft.Xna.Framework;

namespace BattleTest.Components
{
    // holds spawn information
    public class SpawnComponent : IComponent
    {
        public int _spawnCount { get; set; }
        public float _spawnTimer { get; set; }
        public float _currentTimer { get; set; }
        public Point _enemyTargetPoint { get; set; }
        public string[] _spawnOptions { get; set; }
        public Entity[] _spawnTargets { get; set; }
        public Vector2[] _spawnPoints { get; set; }
        public string _faction { get; set; }

        public SpawnComponent(int spawnCount, float spawnTimer, Point enemyTargetPoint,
            string faction, Entity[] spawnTargets, string[] spawnOptions)
        {
            _spawnCount = spawnCount;
            _spawnTimer = spawnTimer;
            _currentTimer = spawnTimer - 2.0f;
            _faction = faction;
            // this is the point that enemies will target with their flow field
            _enemyTargetPoint = enemyTargetPoint;
            _spawnOptions = spawnOptions;
            _spawnTargets = spawnTargets;
        }
    }
}
