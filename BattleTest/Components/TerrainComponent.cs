﻿using Audrey;

namespace BattleTest.Components
{
    // holds terrain information (just cost right now)
    class TerrainComponent : IComponent
    {
        public float _cost;

        public TerrainComponent(float cost)
        {
            _cost = cost;
        }
    }
}
