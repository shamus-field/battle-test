﻿using Audrey;
using BattleTest.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace BattleTest.Maps
{
    public class MapManager
    {
        // terrain map keeps terrain costs for pathfinding and flow field calcs
        public byte[,] _terrainMap;
        // unit map keeps locations for units, value is null if nothing is there
        public Entity[,] _unitMap;
        // combined keeps terrain costs as well as 255 for each cell which has a unit
        // for a* pathfinding
        public byte[,] _combinedMap;
        public int _mapSize;

        public MapManager(int mapSize)
        {
            _terrainMap = new byte[mapSize, mapSize];
            _unitMap = new Entity[mapSize, mapSize];
            _combinedMap = new byte[mapSize, mapSize];
            _mapSize = mapSize;
        }

        // works through each terrain node and comes up with the highest cost based
        // only on terrain tiles. used in flow field calculations
        public void CalcTerrainCosts(Engine engine)
        {
            Family terrain = Family.All(typeof(TerrainComponent)).Get();
            ImmutableList<Entity> terrainEntities = engine.GetEntitiesFor(terrain);

            foreach (Entity te in terrainEntities)
            {
                TerrainComponent nodeTerrain = te.GetComponent<TerrainComponent>();
                PositionComponent nodePos = te.GetComponent<PositionComponent>();

                Point gridPoint = nodePos.toTilePoint();
                _terrainMap[gridPoint.X, gridPoint.Y] = Math.Max(_terrainMap[gridPoint.X, gridPoint.Y], (byte)(nodeTerrain._cost));
                _combinedMap[gridPoint.X, gridPoint.Y] = Math.Max(_combinedMap[gridPoint.X, gridPoint.Y], (byte)(nodeTerrain._cost));
            }
        }

        // adds a unit to the unit map, and the combined map
        public void AddUnit(Entity e, PositionComponent pos)
        {
            Point asTilePoint = pos.toTilePoint();
            _unitMap[asTilePoint.X, asTilePoint.Y] = e;
            _combinedMap[asTilePoint.X, asTilePoint.Y] = 255;
        }

        // updates a unit position in the unit and combined map
        public void UpdateUnit(PositionComponent oldPos, PositionComponent newPos)
        {
            Point oldPoint = oldPos.toTilePoint();
            Point newPoint = newPos.toTilePoint();
            _unitMap[newPoint.X, newPoint.Y] = _unitMap[oldPoint.X, oldPoint.Y];
            _unitMap[oldPoint.X, oldPoint.Y] = null;
            _combinedMap[newPoint.X, newPoint.Y] = 255;
            _combinedMap[oldPoint.X, oldPoint.Y] = _terrainMap[oldPoint.X, oldPoint.Y];
        }

        // updates a unit position in the unit and combined map
        public void UpdateUnit(Point oldPoint, Point newPoint)
        {
            _unitMap[newPoint.X, newPoint.Y] = _unitMap[oldPoint.X, oldPoint.Y];
            _unitMap[oldPoint.X, oldPoint.Y] = null;
            _combinedMap[newPoint.X, newPoint.Y] = 255;
            _combinedMap[oldPoint.X, oldPoint.Y] = _terrainMap[oldPoint.X, oldPoint.Y];
        }

        // removes a unit from the unit and combined map
        public void RemoveUnit(PositionComponent pos)
        {
            Point point = pos.toTilePoint();
            _unitMap[point.X, point.Y] = null;
            _combinedMap[point.X, point.Y] = _terrainMap[point.X, point.Y];
        }
    }
}
