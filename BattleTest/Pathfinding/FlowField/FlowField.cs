﻿using Algorithms;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BattleTest.Pathfinding.FlowField
{
    public class FlowField
    {
        public class Node
        {
            public Point _pos { get; set; }
            public int _distance { get; set; }

            public Node(Point pos, int distance)
            {
                _pos = pos;
                _distance = distance;
            }

            public Vector2 getCenterVector()
            {
                return new Vector2(_pos.X * 32 + 16, _pos.Y * 32 + 16);
            }
        }

        private byte[,] _costGrid;
        private int _costGridXLength;
        private int _costGridYLength;

        public FlowField(byte[,] costGrid)
        {
            _costGrid = costGrid;
            _costGridXLength = costGrid.GetLength(0);
            _costGridYLength = costGrid.GetLength(1);
        }

        public Vector2[,] FindPaths(Point end)
        {
            int?[,] distField = GetDistanceField(end);
            Vector2[,] flowField = GetFlowFieldPoints(distField);

            return flowField;
        }

        public Vector2[,] GetFlowFieldPoints(int?[,] distField)
        {
            Vector2[,] flowField = new Vector2[_costGridXLength, _costGridYLength];

            for (int x = 0; x < _costGridXLength; x++)
            {
                for (int y = 0; y < _costGridYLength; y++)
                {
                    if (distField[x, y] == int.MaxValue)
                    {
                        continue;
                    }

                    Node current = new Node(new Point(x, y), 0);
                    int? minDistance = int.MaxValue;
                    List<Node> neighbors = GetNeighbors(current);

                    Node minNode = null;

                    for (int i = 0; i < neighbors.Count; i++)
                    {
                        Node n = neighbors[i];

                        if (distField[n._pos.X, n._pos.Y] < minDistance)
                        {
                            minNode = n;
                            minDistance = distField[n._pos.X, n._pos.Y];
                        }
                    }

                    if (minNode != null)
                    {
                        flowField[x, y] = minNode.getCenterVector();
                    }
                }
            }

            return flowField;
        }

        //public Vector2[,] GetFlowField(int?[,] distField)
        //{
        //    Vector2[,] flowField = new Vector2[_costGridXLength, _costGridYLength];
        //
        //    for (int x = 0; x < _costGridXLength; x++)
        //    {
        //        for (int y = 0; y < _costGridYLength; y++)
        //        {
        //            if (distField[x, y] == int.MaxValue)
        //            {
        //                continue;
        //            }
        //
        //            Node current = new Node(new Point(x, y), 0);
        //            int? minDistance = int.MaxValue;
        //            List<Node> neighbors = GetNeighbors(current);
        //
        //            Node minNode = null;
        //
        //            for (int i = 0; i < neighbors.Count; i++)
        //            {
        //                Node n = neighbors[i];
        //
        //                if (distField[n._pos.X, n._pos.Y] < minDistance)
        //                {
        //                    minNode = n;
        //                    minDistance = distField[n._pos.X, n._pos.Y];
        //                }
        //            }
        //
        //            if (minNode != null)
        //            { 
        //                flowField[x, y] = (minNode.getCenterVector() -
        //                                   current.getCenterVector());
        //
        //                flowField[x, y].Normalize();
        //            }
        //        }
        //    }
        //
        //    return flowField;
        //}

        public int?[,] GetDistanceField(Point end)
        {
            int?[,] distField = new int?[_costGridXLength, _costGridYLength];

            for (int x = 0; x < _costGridXLength; x++)
            {
                for (int y = 0; y < _costGridYLength; y++)
                {
                    if (_costGrid[x, y] == 255)
                    {
                        distField[x, y] = int.MaxValue;
                    }
                    else
                    {
                        distField[x, y] = null;
                    }
                }
            }
            distField[end.X, end.Y] = 0;

            Node endNode = new Node(end, 0);
            List<Node> openList = new List<Node>();
            openList.Add(endNode);

            while (openList.Count > 0)
            {
                Node current = openList[0];
                openList.RemoveAt(0);

                List<Node> neighbors = GetNeighbors(current);
                for (int i = 0; i < neighbors.Count; i++)
                {
                    Node n = neighbors[i];
                    if (distField[n._pos.X, n._pos.Y] != null)
                    {
                        continue;
                    }

                    distField[n._pos.X, n._pos.Y] = current._distance + _costGrid[n._pos.X, n._pos.Y];
                    n._distance = current._distance + _costGrid[n._pos.X, n._pos.Y];
                    openList.Add(n);
                }
            }
            return distField;
        }

        public List<Node> GetNeighbors(Node current)
        {
            List<Node> neighbors = new List<Node>();

            Point currentPos = current._pos;

            Node left = new Node(new Point(currentPos.X - 1, currentPos.Y), 0);
            Node right = new Node(new Point(currentPos.X + 1, currentPos.Y), 0);
            Node up = new Node(new Point(currentPos.X, currentPos.Y - 1), 0);
            Node down = new Node(new Point(currentPos.X, currentPos.Y + 1), 0);

            bool leftAdded = false;
            bool rightAdded = false;
            bool upAdded = false;
            bool downAdded = false;

            Node upLeft = new Node(new Point(currentPos.X - 1, currentPos.Y - 1), 0);
            Node upRight = new Node(new Point(currentPos.X + 1, currentPos.Y - 1), 0);
            Node downLeft = new Node(new Point(currentPos.X - 1, currentPos.Y + 1), 0);
            Node downRight = new Node(new Point(currentPos.X + 1, currentPos.Y + 1), 0);

            if (left._pos.X >= 0 && left._pos.X < _costGridXLength)
            {
                if (_costGrid[left._pos.X, left._pos.Y] != 255)
                {
                    neighbors.Add(left);
                    leftAdded = true;
                }
            }
            if (right._pos.X >= 0 && right._pos.X < _costGridXLength)
            {
                if (_costGrid[right._pos.X, right._pos.Y] != 255)
                {
                    neighbors.Add(right);
                    rightAdded = true;
                }
            }
            if (up._pos.Y >= 0 && up._pos.Y < _costGridXLength)
            {
                if (_costGrid[up._pos.X, up._pos.Y] != 255)
                {
                    neighbors.Add(up);
                    upAdded = true;
                }
            }
            if (down._pos.Y >= 0 && down._pos.Y < _costGridXLength)
            {
                if (_costGrid[down._pos.X, down._pos.Y] != 255)
                {
                    neighbors.Add(down);
                    downAdded = true;
                }
            }

            if (upLeft._pos.X >= 0 && upLeft._pos.X < _costGridXLength &&
                upLeft._pos.Y >= 0 && upLeft._pos.Y < _costGridYLength)
            {
                if (upAdded && leftAdded)
                {
                    neighbors.Add(upLeft);
                }
            }
            if (upRight._pos.X >= 0 && upRight._pos.X < _costGridXLength &&
                upRight._pos.Y >= 0 && upRight._pos.Y < _costGridYLength)
            {
                if (upAdded && rightAdded)
                {
                    neighbors.Add(upRight);
                }
            }
            if (downLeft._pos.X >= 0 && downLeft._pos.X < _costGridXLength &&
                downLeft._pos.Y >= 0 && downLeft._pos.Y < _costGridYLength)
            {
                if (downAdded && leftAdded)
                {
                    neighbors.Add(downLeft);
                }

            }
            if (downRight._pos.X >= 0 && downRight._pos.X < _costGridXLength &&
                downRight._pos.Y >= 0 && downRight._pos.Y < _costGridYLength)
            {
                if (downAdded && rightAdded)
                {
                    neighbors.Add(downRight);
                }

            }

            return neighbors;
        }
    }
}
