﻿using Audrey;
using BattleTest.Components;
using BattleTest.ScenarioData;
using Microsoft.Xna.Framework;
using PredefinedDwarfs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terrain;

namespace BattleTest.PredefinedEntities.Buildings
{
    // contains premade buildings - move to json eventually
    public class Buildings
    {
        static public Building Tower(Engine engine, string faction, PositionComponent pos)
        {
            Point posTile = pos.toTilePoint();


            Entity[,] entities = new Entity[3, 3];
            entities[0, 0] = TerrainEntities.WallCorner(engine, new PositionComponent(posTile));
            entities[0, 1] = TerrainEntities.WallSide(engine, new PositionComponent(new Point((int)posTile.X, (int)posTile.Y + 1)));
            entities[1, 1] = TerrainEntities.TowerFloor(engine, new PositionComponent(new Point((int)posTile.X + 1, (int)posTile.Y + 1)));
            entities[1, 0] = TerrainEntities.WallSide(engine, new PositionComponent(new Point((int)posTile.X + 1, (int)posTile.Y), (float)(Math.PI / 2)));
            entities[2, 0] = TerrainEntities.WallCorner(engine, new PositionComponent(new Point((int)posTile.X + 2, (int)posTile.Y), (float)(Math.PI / 2)));
            entities[2, 1] = TerrainEntities.WallSide(engine, new PositionComponent(new Point((int)posTile.X + 2, (int)posTile.Y + 1), (float)(Math.PI)));
            entities[1, 2] = TerrainEntities.WallSide(engine, new PositionComponent(new Point((int)posTile.X + 1, (int)posTile.Y + 2), (float)(Math.PI * 3 / 2)));
            entities[0, 2] = TerrainEntities.WallCorner(engine, new PositionComponent(new Point((int)posTile.X, (int)posTile.Y + 2), (float)(Math.PI * 3 / 2)));
            entities[2, 2] = TerrainEntities.WallCorner(engine, new PositionComponent(new Point((int)posTile.X + 2, (int)posTile.Y + 2), (float)(Math.PI)));


            Building building = new Building(entities);
            building.SetBuildingComponents(1000, faction);


            //Entity bowman = DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point((int)posTile.X + 1, (int)posTile.Y + 1)));
            return building;
        }
    }
}
