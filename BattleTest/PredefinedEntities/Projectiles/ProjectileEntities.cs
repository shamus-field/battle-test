﻿using Audrey;
using BattleTest.Components;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace BattleTest.PredefinedEntities.Projectiles
{
    class ProjectileEntities
    {
        static public Entity CrossbowBolt(Engine engine, ProjectileComponent proj,
            bool useImage = true)
        {
            Entity entity = engine.CreateEntity();
            if (useImage)
            {
                entity.AddComponent(new ImageComponent(engine.contentMap["crossbow_bolt.png"]));
            }

            entity.AddComponent(proj);
            return entity;
        }
    }
}
