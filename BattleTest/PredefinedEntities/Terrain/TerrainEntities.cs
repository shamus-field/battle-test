﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

using Audrey;
using BattleTest.Components;
using System.Diagnostics;

namespace Terrain
{
    // predefined terrain - will move to json in next iteration
    class TerrainEntities
    {
        static public Entity Grass(Engine engine, PositionComponent pos)
        {
            Entity entity = engine.CreateEntity();
            entity.AddComponent(new ImageComponent(engine.contentMap["grass1.png"]));
            entity.AddComponent(pos);
            entity.AddComponent(new TerrainComponent(1));
            return entity;
        }

        static public Entity TreeRed(Engine engine, PositionComponent pos)
        {
            Entity entity = engine.CreateEntity();
            entity.AddComponent(new ImageComponent(engine.contentMap["tree2_red.png"]));
            entity.AddComponent(pos);
            entity.AddComponent(new TerrainComponent(255));
            return entity;
        }

        static public Entity WallCorner(Engine engine, PositionComponent pos)
        {
            Entity entity = engine.CreateEntity();
            entity.AddComponent(new ImageComponent(engine.contentMap["corner.png"]));
            entity.AddComponent(pos);
            entity.AddComponent(new TerrainComponent(255));
            return entity;
        }

        static public Entity WallSide(Engine engine, PositionComponent pos)
        {
            Entity entity = engine.CreateEntity();
            entity.AddComponent(new ImageComponent(engine.contentMap["side.png"]));
            entity.AddComponent(pos);
            entity.AddComponent(new TerrainComponent(255));
            return entity;
        }

        static public Entity StoneFloor(Engine engine, PositionComponent pos)
        {
            Entity entity = engine.CreateEntity();
            entity.AddComponent(new ImageComponent(engine.contentMap["floor.png"]));
            entity.AddComponent(pos);
            entity.AddComponent(new TerrainComponent(1));
            return entity;
        }

        static public Entity TowerFloor(Engine engine, PositionComponent pos)
        {
            Entity entity = engine.CreateEntity();
            entity.AddComponent(new ImageComponent(engine.contentMap["floor.png"]));
            entity.AddComponent(pos);
            entity.AddComponent(new TerrainComponent(1));
            return entity;
        }
    }
}
