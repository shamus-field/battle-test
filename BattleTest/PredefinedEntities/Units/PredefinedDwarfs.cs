﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

using Audrey;
using BattleTest.Components;
using BattleTest.AI;
using BattleTest.ScenarioData;

namespace PredefinedDwarfs
{
    // deprecated - remains for one or two test scenarios
    public class DwarfUnits
    {
        static public Entity DwarfFighter(Engine engine, PositionComponent pos = null,
            bool useImage = true)
        {
            Entity entity = engine.CreateEntity();
            if (useImage)
            {
                entity.AddComponent(new ImageComponent(engine.contentMap["dwarf.png"]));
            }

            if (pos != null)
            {
                entity.AddComponent(pos);
            }

            entity.AddComponent(new CombatComponent(250, 400, 1, "dwarf",
                new string[] { "monster" }, new string[] { }, new string[] { }, 20, 1, 25));

            ActionComponent actionList = new ActionComponent();
            actionList.actions = new List<KeyValuePair<UnitAction, float>>();
            //actionList.actions.Add(new KeyValuePair<UnitAction, float>(
            //    new BasicMeleeAttack(), 1.0f));
            entity.AddComponent(actionList);

            return entity;
        }

        static public Entity DwarfCrossbowman(Engine engine, PositionComponent pos = null,
            bool useImage = true)
        {
            Entity entity = engine.CreateEntity();
            if (useImage)
            {
                entity.AddComponent(new ImageComponent(engine.contentMap["dwarf.png"]));
            }

            if (pos != null)
            {
                entity.AddComponent(pos);
            }

            entity.AddComponent(new CombatComponent(120, 400, 1, "dwarf",
                new string[] { "monster" }, new string[] { }, new string[] { }, 20, 1, 120));

            ActionComponent actionList = new ActionComponent();
            actionList.actions = new List<KeyValuePair<UnitAction, float>>();
            //actionList.actions.Add(new KeyValuePair<UnitAction, float>(
            //    new BasicCrossbowAttack(), 1.0f));
            entity.AddComponent(actionList);

            return entity;
        }
    }
}







