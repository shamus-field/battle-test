﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

using Audrey;
using BattleTest.Components;
using BattleTest.AI;
using BattleTest.ScenarioData;

namespace PredefinedMonsters
{
    // deprecated - remains for one or two test scenarios
    public class MonsterUnits
    {
        static public Entity AcidBlob(Engine engine, PositionComponent pos, bool useImage = true)
        {
            Entity entity = engine.CreateEntity();

            if (useImage)
            {
                entity.AddComponent(new ImageComponent(engine.contentMap["acid_blob.png"]));
            }

            entity.AddComponent(pos);
            entity.AddComponent(new CombatComponent(150, 400, 1, "monster",
                new string[] { "dwarf" }, new string[] { }, new string[] { }, 20, 1, 15));

            ActionComponent actionList = new ActionComponent();
            actionList.actions = new List<KeyValuePair<UnitAction, float>>();
            //actionList.actions.Add(new KeyValuePair<UnitAction, float>(
            //    new BasicMeleeAttack(), 1.0f));
            entity.AddComponent(actionList);

            return entity;
        }
    }
}







