﻿using Audrey;
using BattleTest.Components;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Diagnostics;

namespace BattleTest.Processors
{
    // handles all projectile entity movement
    class ProjectileProcessor
    {
        public void processProjectiles(Engine engine, GameTime gameTime)
        {
            Family projectiles = Family.All(typeof(ProjectileComponent)).Get();
            ImmutableList<Entity> projEntities = engine.GetEntitiesFor(projectiles);

            List<Entity> removed = new List<Entity>();

            foreach (Entity p in projEntities)
            {
                ProjectileComponent proj = p.GetComponent<ProjectileComponent>();
                proj._pos += proj._normalized * proj._speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

                // might be better performace to just measure the xy? needs testing
                float distance = Vector2.Distance(proj._pos, proj._target);
                if (distance <= 5)
                {
                    removed.Add(p);
                }
            }

            foreach (Entity e in removed)
            {
                engine.DestroyEntity(e);
            }
        }
    }
}
