﻿using System.Linq;
using Audrey;
using System.Diagnostics;
using System.Collections.Generic;
using Algorithms;
using Microsoft.Xna.Framework;
using BattleTest.Components;
using static BattleTest.Components.CombatComponent;
using BattleTest.Maps;
using BattleTest.ScenarioData;
using BattleTest.AI;
using BattleTest.State;
using BattleTest.Utilities;
using System;

namespace BattleTest.Processors
{
    // handles AI for all units (may have separate ais for different unit types eventually)
    class UnitAI2Processor
    {
        public void RunCombatAI(BattleState battleState)
        {
            Family combatants = Family.All(typeof(CombatComponent)).Get();
            ImmutableList<Entity> combatEntities = battleState._engine.GetEntitiesFor(combatants);
            List<Entity> removeEntities = new List<Entity>();
            MapUtil mapUtil = new MapUtil();

            foreach (Entity actor in combatEntities)
            {
                CombatComponent actorCombat = actor.GetComponent<CombatComponent>();
                PositionComponent actorPos = actor.GetComponent<PositionComponent>();
                ActionComponent actorActions = actor.GetComponent<ActionComponent>();

                if (actorCombat._slain)
                {
                    continue;
                }

                // reference, unneeded for now
                /*
                if (comp._enemyTarget != null)
                {
                    CombatComponent targetComp = comp._enemyTarget.GetComponent<CombatComponent>();
                    if (targetComp._slain)
                    {
                        comp._enemyTarget = null;
                        comp._mode = comp._originalMode;
                        comp._currentPath = null;
                        comp.currentFlowTarget = Vector2.Zero;
                    }
                }
                */

                // make sure they are allowed to act right now
                bool takeAction = false;
                if (actorCombat._mode != Mode.Attack)
                {
                    if (actorCombat.possibleTargets.Count == 0)
                    {
                        if (actorCombat._turnTicks > 0)
                        {
                            actorCombat._turnTicks -= 1;
                            continue;
                        }
                        else
                        {
                            actorCombat._turnTicks = actorCombat._startTurnTicks;
                            takeAction = true;
                        }
                    }
                }
                else
                {
                    if (actorCombat._actionTicks > 0)
                    {
                        actorCombat._actionTicks -= 1;
                        continue;
                    }
                    else
                    {
                        actorCombat._actionTicks = actorCombat._startActionTicks;
                        takeAction = true;
                    }
                }

                // if they aren't ready, skip to next unit
                if (!takeAction)
                {
                    continue;
                }

                // design choice - better for a unit to choose a random action and see if something
                // is in range (this would allow for better skirmisher/advancing javelin/melee units) or to always
                // always pick an action for any target in range? currently doing the former, should 
                // test and see which feels better though
                Entity target = null;
                UnitAction chosen = actorActions.ChooseAction();
                if (chosen._range == 1)
                {
                    List<Point> tilesInRange = mapUtil.GetTilesInSquare(actorPos.toTilePoint(), 1, battleState.GetMapSize());
                    target = FindSingleTarget(actorCombat._enemies, actor, battleState.GetUnitMap(), tilesInRange);
                }
                else if (chosen._range > 1)
                {
                    List<Point> tilesInRange = mapUtil.GetTilesInRange(actorPos.toTilePoint(), chosen._range, battleState.GetMapSize());
                    target = FindSingleTarget(actorCombat._enemies, actor, battleState.GetUnitMap(), tilesInRange);
                }
                //else zero case for self actions?

                if (target != null)
                {
                    actorCombat._mode = Mode.Attack;
                    //comp._enemyTarget = target;

                    chosen.Perform(battleState._engine, actor, new Entity[1] { target });

                    CombatComponent targetComp = target.GetComponent<CombatComponent>();
                    if (targetComp._curHealth <= 0)
                    {
                        targetComp._slain = true;
                        removeEntities.Add(target);
                        battleState.RemoveUnit(target, false);
                    }
                    continue;
                }
                else
                {
                    actorCombat._mode = actorCombat._originalMode;
                    actorCombat._enemyTarget = null;

                    if (actorCombat._mode == Mode.Move)
                    {
                        List<Point> tilesInRange = mapUtil.GetTilesInRange(actorPos.toTilePoint(), 4, battleState.GetMapSize());
                        target = FindSingleTarget(actorCombat._enemies, actor, battleState.GetUnitMap(), tilesInRange);
                        if (target != null)
                        {

                            Point start = actorPos.toTilePoint();
                            PositionComponent targetPos = target.GetComponent<PositionComponent>();
                            Point end = targetPos.toTilePoint();

                            // set start and end points to 0, else pathfinder will
                            // consider the path unfindable
                            battleState.SetCombinedMapValue(start.X, start.Y, 1);
                            battleState.SetCombinedMapValue(end.X, end.Y, 1);
                            
                            // find the path, if available
                            PathFinderFast finder = new PathFinderFast(battleState.GetCombinedMap());
                            finder.SearchLimit = 64 * 64 * 2;
                            finder.HeavyDiagonals = true;
                            List<PathFinderNode> path = finder.FindPath(actorPos.toCenterTilePoint(), targetPos.toCenterTilePoint());

                            //reset the two map values
                            battleState.SetCombinedMapValue(start.X, start.Y, 255);
                            battleState.SetCombinedMapValue(end.X, end.Y, 255);

                            if (path != null)
                            {
                                // update unit position, just be careful not to use any further
                                // reference to the old position component below!
                                path.RemoveAt(path.Count - 1);
                                path.Reverse();
                                actorCombat._currentPath = path;

                                Point newPoint = new Point(path[0].X, path[0].Y);
                                battleState.UpdateUnitPosition(actor, newPoint);
                                actorPos.SetFromTilePoint(newPoint);
                                continue;
                            }
                        }
                        // else do nothing...for now

                    }

                    // check nearest ~4 tiles
                    // if found, do small path and go towards 
                    // if nothing, do long pathfind in bigger squares?
                    // pathfind to nearest target? or location? guess battle type depends
                    // optimize more later if needed
                    if (battleState._battleType == BattleState.BattleType.Field)
                    {
                        // for now just get the first enemy spawn point
                        // will need to modify for >2 faction battles if they ever appear
                        // will crash if faction has no enemies, but I can't see a case for that
                        // in a battle?
                        Point moveTowards = battleState.GetSpawnPoint(actorCombat._enemies[0]);
                        Point actorCompare = actorPos.toTilePoint();
                        Dictionary<Point, int> distances = new Dictionary<Point, int>();

                        // TODO - remove the bound call here, causing a bug where units "jump" a tile
                        // when they are right on the edge of the map

                        // build a system where you divide battlefield into x spots to move towards
                        // but what to do when your side wins one point?
                        // or a waypoint system maybe?

                        // check the ys
                        actorCompare.Y = MapUtil.Bound(actorCompare.Y - 1, battleState.GetMapSize());
                        distances[actorCompare] = Util.DistSquared(actorCompare, moveTowards);
                        actorCompare.Y = MapUtil.Bound(actorCompare.Y + 2, battleState.GetMapSize());
                        distances[actorCompare] = Util.DistSquared(actorCompare, moveTowards);
                        actorCompare.Y = actorCompare.Y - 1;
                        // check the xs
                        actorCompare.X = MapUtil.Bound(actorCompare.X - 1, battleState.GetMapSize());
                        distances[actorCompare] = Util.DistSquared(actorCompare, moveTowards);
                        actorCompare.X = MapUtil.Bound(actorCompare.X + 2, battleState.GetMapSize());
                        distances[actorCompare] = Util.DistSquared(actorCompare, moveTowards);

                        // move to an tile up/down/ left or right depending on which is closest
                        // to target move point
                        int i = 0;
                        foreach (var item in distances.OrderBy(key => key.Value))
                        {
                            if (battleState.CheckCombinedTileCost(item.Key) < 255)
                            {
                                battleState.UpdateUnitPosition(actor, item.Key);
                                actorPos.SetFromTilePoint(item.Key);
                                break;
                            }

                            if (i >= 2)
                            {
                                break;
                            }
                            i++;
                        }
                    }
                }
            }

            // clean up any killed entities
            foreach (Entity e in removeEntities)
            {
                battleState.DestroyEntity(e);
            }
            removeEntities.Clear();
        }


        public Entity FindSingleTarget(string[] factions, Entity actor, Entity[,] unitMap, List<Point> tilesToCheck)
        {
            // TODO randomize tiles to check
            for (int i = 0; i < tilesToCheck.Count; i++)
            {
                Point tile = tilesToCheck[i];
                if (unitMap[tile.X, tile.Y] != null)
                {
                    Entity e = unitMap[tile.X, tile.Y];
                    CombatComponent checkCombat = e.GetComponent<CombatComponent>();
                    if (checkCombat._slain)
                    {
                        continue;
                    }

                    if (!factions.Contains(checkCombat._faction))
                    {
                        continue;
                    }
                    return e;
                }
            }
            return null;
        }
    }
}
