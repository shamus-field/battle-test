﻿using Audrey;
using BattleTest.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleTest.ScenarioData
{
    // builds a building entity out of a few tiles. not sure if this is how I'll want to handle
    // buildings yet or not
    public class Building
    {
        public Entity[,] _tileEntities { get; set; }
        public Entity _spawnEntity { get; set; }

        public Building(Entity[,] entities)
        {
            _tileEntities = entities;
        }

        public void SetBuildingComponents(int maxHealth, string faction)
        {
            for (int x = 0; x < _tileEntities.GetLength(0); x++)
            {
                for (int y = 0; y < _tileEntities.GetLength(1); y++)
                {
                    Entity e = _tileEntities[x, y];
                    BuildingComponent b = new BuildingComponent(maxHealth, faction);
                }
            }
        }
    }
}
