﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleTest.ScenarioData
{
    // contains faction matching and loading
    public class Faction
    {
        public string[] _enemies;
        public string[] _allies;
        public string[] _neutral;

        public Faction(JArray enemies, JArray allies, JArray neutral)
        {
            _enemies = enemies.ToObject<string[]>();
            _allies = allies.ToObject<string[]>();
            _neutral = neutral.ToObject<string[]>();
        }
    }

    public class ScenarioJSONLoader
    {
        public Dictionary<string, Faction> loadFactionJSONData(string scenarioJSON)
        {
            //sanitize this
            JObject inputDict = (JObject)JsonConvert.DeserializeObject(scenarioJSON);
            JObject factionDict = (JObject)inputDict.GetValue("factions");

            Dictionary<string, Faction> factions = new Dictionary<string, Faction>();
            foreach (JProperty prop in factionDict.Properties())
            {
                Faction f = new Faction(
                    (JArray)prop.Value["enemies"],
                    (JArray)prop.Value["allies"],
                    (JArray)prop.Value["neutral"]
                );
                factions.Add(prop.Name, f);
            }
            return factions;
        }
    }
}
