﻿using Audrey;
using BattleTest.AI;
using BattleTest.Components;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleTest.ScenarioData
{
    // contains unit information and loading...gotta redo this a bit it's a little ugly how
    // it handles setting values and component creation in one big method
    public class Unit
    {
        public string _name;
        public ImageComponent _img;
        public CombatComponent _combat;
        public ActionComponent _action;

        public Unit()
        {

        }

        public Unit(Engine engine, Dictionary<string, UnitAction> actionDict,
            string name, JToken unitJSON)
        {
            _name = name;

            string image = (string)unitJSON["image"];
            _img = new ImageComponent(engine.contentMap[image]);

            int health = (int)unitJSON["combat"]["health"];
            int sightRange = (int)unitJSON["combat"]["sight_range"];
            int size = (int)unitJSON["combat"]["size"];
            int speed = (int)unitJSON["combat"]["speed"];
            int turnTicks = (int)unitJSON["combat"]["turn_ticks"];
            int actionTicks = (int)unitJSON["combat"]["action_ticks"];

            _combat = new CombatComponent(health, sightRange, size, speed, turnTicks, actionTicks);

            JObject jsonActions = (JObject)unitJSON["actions"];

            ActionComponent actionList = new ActionComponent();
            actionList.actions = new List<KeyValuePair<UnitAction, float>>();
            foreach (JProperty prop in jsonActions.Properties())
            {
                float chance = (float)prop.Value / 100.0f;
                actionList.actions.Add(new KeyValuePair<UnitAction, float>(
                    actionDict[prop.Name], chance));
            }

            _action = actionList;
        }

        // gets a copy of a unit to assign to a new unit
        public Unit getCopy()
        {
            Unit u = new Unit();

            ImageComponent img = new ImageComponent(_img._texture);
            u._img = img;

            CombatComponent combat = new CombatComponent(_combat._maxHealth, _combat._sightRange,
                _combat._size, _combat._speed, _combat._turnTicks, _combat._actionTicks);
            u._combat = combat;

            ActionComponent actions = new ActionComponent();
            actions.actions = new List<KeyValuePair<UnitAction, float>>();
            for (int i = 0; i < _action.actions.Count; i++)
            {
                KeyValuePair<UnitAction, float> uaSource = _action.actions[i];
                UnitAction ua = new UnitAction(uaSource.Key._range, uaSource.Key._damage,
                    uaSource.Key._effect);
                actions.actions.Add(new KeyValuePair<UnitAction, float>(ua, uaSource.Value));
            }
            u._action = actions;
            return u;
        }
    }

    public class UnitJSONLoader
    {
        public Dictionary<string, Unit> loadJSONData(string unitJSON, Engine engine,
            Dictionary<string, UnitAction> actionDict)
        {
            //sanitize this
            JObject inputDict = (JObject)JsonConvert.DeserializeObject(unitJSON);

            Dictionary<string, Unit> units = new Dictionary<string, Unit>();
            foreach (JProperty prop in inputDict.Properties())
            {
                Unit u = new Unit(engine, actionDict, prop.Name, prop.Value);
                units.Add(prop.Name, u);
            }
            return units;
        }
    }
}
