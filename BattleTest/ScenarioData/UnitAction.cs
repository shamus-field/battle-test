﻿using Audrey;
using BattleTest.Components;
using BattleTest.PredefinedEntities.Projectiles;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleTest.ScenarioData
{
    // unit actions, performing them and loading
    public class UnitAction
    {
        public int _range { get; set; }
        public int _damage { get; set; }
        public string _effect { get; set; }

        public UnitAction(int range, int damage, string effect)
        {
            _range = range;
            _damage = damage;
            _effect = effect;
        }

        public void Perform(Engine engine, Entity source, Entity[] targets)
        {
            foreach (Entity e in targets)
            {
                CombatComponent targetComb = e.GetComponent<CombatComponent>();
                targetComb._curHealth -= _damage;

                if (_effect != null)
                {
                    if (_effect == "crossbow_bolt")
                    {
                        PositionComponent targetPos = e.GetComponent<PositionComponent>();
                        PositionComponent sourcePos = source.GetComponent<PositionComponent>();
                        ProjectileEntities.CrossbowBolt(engine,
                            new ProjectileComponent(sourcePos._point.X + 16, sourcePos._point.Y + 16,
                                targetPos._point.X + 16, targetPos._point.Y + 16));
                    }
                }
            }
        }
    }

    public class UnitActionJSONLoader
    {
        public Dictionary<string, UnitAction> loadJSONData(string actionJSON, Engine engine)
        {
            //sanitize this
            JObject inputDict = (JObject)JsonConvert.DeserializeObject(actionJSON);

            Dictionary<string, UnitAction> actions = new Dictionary<string, UnitAction>();
            foreach (JProperty prop in inputDict.Properties())
            {
                UnitAction ua = new UnitAction(
                    (int)prop.Value["range"],
                    (int)prop.Value["damage"],
                    (string)prop.Value["effect"]
                );
                actions[prop.Name] = ua;
            }
            return actions;
        }
    }
}



