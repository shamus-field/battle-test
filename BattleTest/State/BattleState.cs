﻿using Audrey;
using BattleTest.Components;
using BattleTest.Maps;
using BattleTest.ScenarioData;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleTest.State
{
    // contains state information about battles, handles misc battle stuff like spawning
    // units
    public class BattleState
    {
        MapManager _mapManager;
        // TODO when moving terrain into json, make this no longer public
        // the test scenarios use it
        public Engine _engine;
        Dictionary<string, Unit> _unitDict;
        Dictionary<string, UnitAction> _actionDict;
        Dictionary<string, Faction> _factionDict;
        Dictionary<string, Point> _spawnPoints = new Dictionary<string, Point>();

        // other battle info like reinforcement counts, tactics, etc
        public enum BattleType { Field, Siege };
        public BattleType _battleType { get; }
        public int maxTroops = 200;
        public Entity[] friendlyArmy;
        public Entity[] enemyArmy;
        public Entity[] ownArmy;


        public BattleState(Engine engine, MapManager mapManager, 
            Dictionary<string, Unit> unitDict, 
            Dictionary<string, UnitAction> actionDict, 
            Dictionary<string, Faction> factionDict,
            BattleType battleType = BattleType.Field)
        {
            _mapManager = mapManager;
            _engine = engine;
            _unitDict = unitDict;
            _actionDict = actionDict;
            _factionDict = factionDict;
            _battleType = battleType;
        }

        public Entity[,] GetUnitMap()
        {
            return _mapManager._unitMap;
        }

        public byte[,] GetCombinedMap()
        {
            return _mapManager._combinedMap;
        }

        public void SetCombinedMapValue(int X, int Y, byte value)
        {
            _mapManager._combinedMap[X, Y] = value;
        }

        public void SetFactionSpawnPoint(string faction, Point point)
        {
            _spawnPoints[faction] = point;
        }

        public Point GetSpawnPoint(string faction)
        {
            return _spawnPoints[faction];
        }

        public byte CheckCombinedTileCost(Point toCheck)
        {
            return _mapManager._combinedMap[toCheck.X, toCheck.Y];
        }

        public int GetMapSize()
        {
            return _mapManager._mapSize;
        }

        public void SpawnUnit(string unit, string factionStr, Point spawnPoint)
        {
            Entity s = _engine.CreateEntity();
            Unit toSpawn = _unitDict[unit].getCopy();
            s.AddComponent(toSpawn._img);
            s.AddComponent(toSpawn._action);
            s.AddComponent(toSpawn._combat);

            CombatComponent comb = s.GetComponent<CombatComponent>();
            Faction faction = _factionDict[factionStr];
            comb._enemies = faction._enemies;
            comb._allies = faction._allies;
            comb._neutral = faction._neutral;
            comb._faction = factionStr;
            comb._originalMode = CombatComponent.Mode.Move;

            PositionComponent pos = new PositionComponent(spawnPoint);
            s.AddComponent(pos);
            _mapManager.AddUnit(s, pos);
        }

        public void RemoveUnit(Entity toRemove, bool removeFromEngine)
        {
            PositionComponent targetPos = toRemove.GetComponent<PositionComponent>();
            _mapManager.RemoveUnit(targetPos);

            if (removeFromEngine)
            {
                _engine.DestroyEntity(toRemove);
            }
        }

        public void DestroyEntity(Entity toDestroy)
        {
            _engine.DestroyEntity(toDestroy);
        }

        public void UpdateUnitPosition(Entity toUpdate, Point newPoint)
        {
            Point oldPos = toUpdate.GetComponent<PositionComponent>().toTilePoint();
            _mapManager.UpdateUnit(oldPos, newPoint);
            // update the actor pos here instead?
        }
        
    }
}
