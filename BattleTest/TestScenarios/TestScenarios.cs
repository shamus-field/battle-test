﻿using Audrey;
using BattleTest.Components;
using BattleTest.PredefinedEntities.Buildings;
using Microsoft.Xna.Framework;
using PredefinedDwarfs;
using PredefinedMonsters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terrain;
using BattleTest.ScenarioData;
using BattleTest.Maps;
using BattleTest.State;

namespace BattleTest.TestScenarios
{
    // just contains some test scenarios for checking unit AI, pathfinding, map costs, spawning,
    // etc. will move to json
    class TestScenariosTypes
    {
        public void SpawnTest(Engine engine, Dictionary<string, Unit> unitDict,
            Dictionary<string, Faction> factionDict)
        {
            //Buildings.Tower(engine, new PositionComponent(new Point(5, 10)));
            //Buildings.Tower(engine, new PositionComponent(new Point(42, 10)));


            Building spawnBuilding1 = Buildings.Tower(engine, "left", new PositionComponent(new Point(5, 10)));
            Building spawnBuilding2 = Buildings.Tower(engine, "right", new PositionComponent(new Point(42, 10)));

            Entity spawn1 = engine.CreateEntity();
            Entity spawn2 = engine.CreateEntity();
            spawn1.AddComponent(new SpawnComponent(9, 5, new Point(8, 11), "left",
                new Entity[] { spawn2 },
                new string[] { "dwarf_fighter", "dwarf_crossbowman", "acid_blob" }));
            SpawnComponent spa1 = spawn1.GetComponent<SpawnComponent>();
            spa1._spawnPoints = new Vector2[] { new Vector2(8, 10), new Vector2(8, 11), new Vector2(8, 12) };

            spawn2.AddComponent(new SpawnComponent(9, 5, new Point(41, 11), "right",
                new Entity[] { spawn1 },
                new string[] { "dwarf_fighter", "acid_blob", "dwarf_crossbowman" }));
            SpawnComponent spa2 = spawn2.GetComponent<SpawnComponent>();
            spa2._spawnPoints = new Vector2[] { new Vector2(41, 10), new Vector2(41, 11), new Vector2(41, 12) };

            spawnBuilding1._spawnEntity = spawn1;
            spawnBuilding2._spawnEntity = spawn2;
        }

        public void AStarTest(Engine engine)
        {
            TerrainEntities.TreeRed(engine, new PositionComponent(128, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(96, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(160, 96));
            TerrainEntities.TreeRed(engine, new PositionComponent(160, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(128, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(704, 64));
            TerrainEntities.TreeRed(engine, new PositionComponent(704, 96));
            TerrainEntities.TreeRed(engine, new PositionComponent(704, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(704, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(672, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(640, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(608, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 64));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 96));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(608, 64));
            TerrainEntities.TreeRed(engine, new PositionComponent(672, 64));
            TerrainEntities.TreeRed(engine, new PositionComponent(320, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(352, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(320, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(352, 160));

            DwarfUnits.DwarfFighter(engine, new PositionComponent(64, 64));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(64, 100));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(320, 192));
            MonsterUnits.AcidBlob(engine, new PositionComponent(170, 290));
            MonsterUnits.AcidBlob(engine, new PositionComponent(140, 260));
            MonsterUnits.AcidBlob(engine, new PositionComponent(200, 350));
            MonsterUnits.AcidBlob(engine, new PositionComponent(640, 128));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(640, 192));

            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 13)));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 14)));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 15)));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 16)));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 17)));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 18)));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 19)));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 20)));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 21)));
            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(37, 22)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(36, 22)));

            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(41, 13)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(43, 14)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(42, 14)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(38, 17)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(39, 17)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(41, 19)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(43, 18)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(45, 20)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(46, 21)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(45, 22)));

            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 13)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 14)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 15)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 16)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 17)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 18)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 19)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 20)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 21)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(47, 22)));

            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(4, 24)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(4, 22)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(3, 23)));
            TerrainEntities.TreeRed(engine, new PositionComponent(new Point(5, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(4, 23)));

            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(1, 20)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(2, 20)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(3, 20)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(4, 20)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(5, 20)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(6, 20)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(7, 20)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(1, 21)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(1, 22)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(1, 23)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(1, 24)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(1, 25)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(1, 26)));

            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(1, 26)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(2, 26)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(3, 26)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(4, 26)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(5, 26)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(6, 26)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(7, 26)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(7, 21)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(7, 22)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(7, 23)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(7, 24)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(7, 25)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(7, 26)));

            //TerrainEntities.WallCorner(engine, new PositionComponent(new Point(20, 20)));
            //TerrainEntities.WallSide(engine, new PositionComponent(new Point(20, 21)));
            //TerrainEntities.StoneFloor(engine, new PositionComponent(new Point(21, 21)));
            //TerrainEntities.WallSide(engine, new PositionComponent(new Point(21, 20), (float)(Math.PI / 2)));
            //TerrainEntities.WallCorner(engine, new PositionComponent(new Point(22, 20), (float)(Math.PI / 2)));
            //TerrainEntities.WallSide(engine, new PositionComponent(new Point(22, 21), (float)(Math.PI)));
            //TerrainEntities.WallCorner(engine, new PositionComponent(new Point(20, 22), (float)(Math.PI * 3 / 2)));
            //TerrainEntities.WallCorner(engine, new PositionComponent(new Point(22, 22), (float)(Math.PI)));
            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(21, 21)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(21, 18)));

            //Entity spawn1 = Buildings.Tower(engine, new PositionComponent(new Point(25, 21)));
            //spawn1.AddComponent(new SpawnComponent(9, 30, new Entity[] { DwarfUnits.DwarfCrossbowman(engine) }));


            //Entity spawn2 = Buildings.Tower(engine, new PositionComponent(new Point(20, 20)));
            //spawn1.AddComponent(new SpawnComponent(9, 30, new Entity[] { DwarfUnits.DwarfCrossbowman(engine) }));


            DwarfUnits.DwarfCrossbowman(engine, new PositionComponent(new Point(21, 21)));
        }

        public void FlowFieldTest(Engine engine)
        {
            TerrainEntities.TreeRed(engine, new PositionComponent(128, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(96, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(160, 96));
            TerrainEntities.TreeRed(engine, new PositionComponent(160, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(128, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(704, 64));
            TerrainEntities.TreeRed(engine, new PositionComponent(704, 96));
            TerrainEntities.TreeRed(engine, new PositionComponent(704, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(704, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(672, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(640, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(608, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 64));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 96));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(576, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(608, 64));
            TerrainEntities.TreeRed(engine, new PositionComponent(672, 64));
            TerrainEntities.TreeRed(engine, new PositionComponent(320, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(352, 128));
            TerrainEntities.TreeRed(engine, new PositionComponent(320, 160));
            TerrainEntities.TreeRed(engine, new PositionComponent(352, 160));

            DwarfUnits.DwarfFighter(engine, new PositionComponent(new Point(21, 8)));
            MonsterUnits.AcidBlob(engine, new PositionComponent(new Point(19, 8)));
        }

        public void AI2Test(BattleState battleState)
        {
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(128, 128));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(96, 160));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(160, 96));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(160, 128));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(128, 160));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(704, 64));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(704, 96));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(704, 128));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(704, 160));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(672, 160));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(640, 160));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(608, 160));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(576, 160));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(576, 64));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(576, 96));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(576, 128));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(576, 160));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(608, 64));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(672, 64));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(320, 128));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(352, 128));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(320, 160));
            TerrainEntities.TreeRed(battleState._engine, new PositionComponent(352, 160));

            battleState.SetFactionSpawnPoint("left", new Point(10, 32));
            battleState.SetFactionSpawnPoint("right", new Point(54, 32));
            
            for (int y = 0; y < 64; y++)
            {
                battleState.SpawnUnit("dwarf_crossbowman", "left", new Point(1, y));
                battleState.SpawnUnit("acid_blob", "left", new Point(2, y));
                battleState.SpawnUnit("dwarf_crossbowman", "right", new Point(62, y));
                battleState.SpawnUnit("acid_blob", "right", new Point(61, y));
            }
            
            //battleState.SpawnUnit("dwarf_crossbowman", "left", new Point(0, 20));
            //battleState.SpawnUnit("acid_blob", "left", new Point(1, 20));

            //battleState.SpawnUnit("dwarf_crossbowman", "left", new Point(19, 8));
            //battleState.SpawnUnit("acid_blob", "right", new Point(21, 8));
        }
    }
}
